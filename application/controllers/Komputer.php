<?php

require APPPATH . '/libraries/REST_Controller.php';

class Komputer extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Komputer_model", "komputer");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');

        if ($cari != "") {
            $komputer = $this->komputer->cari($cari)->result();
        } else if($id == ''){
            $komputer = $this->komputer->getData(null)->result();
        } else {
            $komputer = $this->komputer->getData($id)->result();
        }
        
        $this->response($komputer);
    }

    public function index_put()
    {
        $image = $this->put('image');
        $name = $this->put('name');
        $price = $this->put('price');
        $stok = $this->put('stok');
        $description = $this->put('description');
        $id = $this->put('id');
        $data = array(
            'name' => $name,
            'price' => $price,
            'image' => $image,
            'stok' => $stok,
            'description' => $description,

        );
        $update = $this->komputer->update('products', $data, 'product_id', $this->put('product_id'));

        if ($update) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_post()
    {
        $image = $this->post('image');
        $name = $this->post('name');
        $price = $this->post('price');
        $stok = $this->post('stok');
        $description = $this->post('description');
        $product_id = $this->post('product_id');
        $data = array(
            'product_id' => $product_id,
            'name' => $name,
            'price' => $price,
            'image' => $image,
            'stok' => $stok,
            'description' => $description,

        );
        $insert = $this->komputer->insert($data);

        if ($insert) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
    public function index_delete()
    {
      $id = $this->delete('product_id');
      $delete = $this->komputer->delete('products', 'product_id', $id);
      if ($delete) {
          $this->response(array('status' => 'success'), 201);
      } else {
          $this->response(array('status' => 'fail', 502));
      }
      
    }
}